module.exports = {
    extends: [
      'eslint:recommended',
      'plugin:import/errors',
      'plugin:import/warnings',
    ],
    plugins: ['import'],
    rules: {
      'import/no-unresolved': 'error', // Marca los imports faltantes como errores
    },
  };
  