import React, { useEffect, useState } from 'react'
import Whoiam from '../components/whoiam'
import Profile from '../components/profile'
import Content from '../components/content'
import Fondo from '../components/fondo'

const Portfolio = () => {
	return (
		<div className="w-full h-full">
			<div id="conte-padre" className=" relative text-slate-200">
				<Fondo />
				<Whoiam />
				<div className=" bg-slate-900 bg-opacity-90 grid grid-cols-1 sm:grid-cols-2 md:px-12 md:py-16 lg:px-20 lg:py-0">
					<Profile />
					<Content />
				</div>
			</div>
		</div>
	)
}

export default Portfolio
