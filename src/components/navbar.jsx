import React, { useEffect, useState } from "react";

export const Navbar = () => {
  const labels = ["Experiencia", "Proyectos", "Tecnologías"];
  const refs = ["#Experiencia", "#Proyectos", "#Tecnologías"];

  const [currentSection, setCurrentSection] = useState("profile");

  useEffect(() => {
    const handleScroll = () => {
      const scrollPosition = window.scrollY;

      const sections = document.querySelectorAll("section");
      sections.forEach((section) => {
        const top = section.offsetTop;
        const bottom = top + section.offsetHeight;

        if (scrollPosition >= top && scrollPosition < bottom) {
          if (currentSection !== section.id) {
            setCurrentSection(section.id);
          }
        }
      });

      window.lastScrollPosition = scrollPosition;
    };

    window.addEventListener("scroll", handleScroll);
    return () => window.removeEventListener("scroll", handleScroll);
  }, [currentSection]);

  function redirect(section) {
    var contenedor = document.getElementById(section);
    if (contenedor) {
      setCurrentSection(section);

      contenedor.scrollIntoView({
        behavior: "smooth",
        block: "start",
        inline: "start",
      });
    }
  }

  return (
    <div className="hidden lg:block" id="navbar">
      <div className="w-[40%] flex flex-col gap-6 uppercase font-bold text-sm py-2 ">
        {labels.map((label, index) => (
          <a
            key={index}
            onClick={() => redirect(label)}
            id={label + "-nav"}
            className="flex items-center group "
          >
            <span
              className={`${
                currentSection === label
                  ? "h-[1px] w-[4rem] bg-slate-50 "
                  : "h-[1px] w-[2rem] bg-slate-500"
              }  block mr-4 motion-reduce:transition-none transition-all duration-300 group-hover:w-[4rem] group-hover:bg-slate-50`}
            />
            <span
              className={`${
                currentSection === label
                  ? "  text-slate-50"
                  : "  text-slate-500"
              }  font-bold nav-text tracking-widest text-xs group-hover:text-slate-50  motion-reduce:transition-none transition-all duration-300 `}
            >
              {label}
            </span>
          </a>
        ))}
      </div>
    </div>
  );
};

export default Navbar;
