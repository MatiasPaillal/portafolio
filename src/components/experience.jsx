import React, { useEffect, useState } from 'react'
import { MdArrowOutward } from 'react-icons/md'
import { FaArrowRight } from 'react-icons/fa'
import { Link } from 'react-router-dom'
import ExperienceCard from './experience/ExperienceCard'
import { collection, addDoc, getDocs } from 'firebase/firestore'
import { db, storage } from '../firebase/config'
import { getDownloadURL, ref } from 'firebase/storage'
import ExperienceCarousel from './experience/ExperienceCarousel'
export const Experience = () => {
	const [experiences, setExperiences] = useState([])
	const [selectedExperience, setSelectedExperience] = useState({})
	const [isOpen, setIsOpen] = useState(false)
	const [isLoading, setIsLoading] = useState(false)

	const handleSetIsOpen = (value) => {
		setIsOpen(value)
	}

	const handleSetSelectedExperience = (experience) => {
		setSelectedExperience(experience)
	}

	useEffect(() => {
		setIsLoading(true)
		const getExperiences = async () => {
			try {
				const querySnapshot = await getDocs(collection(db, 'experiences'))

				// Mapeamos cada documento a una promesa que obtendrá los datos
				const experiencesPromises = querySnapshot.docs.map(async (doc) => {
					let imagen = ''
					const imgRef = ref(
						storage,
						`experiences/${doc.data().id}/practica.png`
					)

					try {
						imagen = await getDownloadURL(imgRef)
					} catch (error) {
						console.error('Error obteniendo imagen:', error)
					}

					return { ...doc.data(), image: imagen }
				})

				// Esperamos a que todas las promesas se resuelvan
				const experiencesResponse = await Promise.all(experiencesPromises)
				const sortExperiences = experiencesResponse.sort((a, b) => b.id - a.id)
				setExperiences(sortExperiences)
                setSelectedExperience(sortExperiences[0])
			} catch (error) {
				console.error('Error obteniendo experiencias:', error)
			}
		}

		getExperiences().finally(() => {
			setIsLoading(false)
		})
	}, [])
	if (isLoading) {
		return (
			<div className="w-full h-full flex items-center justify-center">
				<p className="text-slate-200">Cargando...</p>
			</div>
		)
	}
	return (
		<section className="w-full h-full gap-4" id="Experiencia">
			<ExperienceCarousel
				experiences={experiences}
				selectedExperience={selectedExperience}
				setSelectedExperience={handleSetSelectedExperience}
				setIsOpen={handleSetIsOpen}
				isOpen={isOpen}
			/>
			<div className="w-full flex flex-col gap-5 mt-16" data-aos="fade-down">
				{experiences.map((experience, index) => (
					<ExperienceCard
						setSelectedExperience={handleSetSelectedExperience}
						setIsOpen={handleSetIsOpen}
						key={index}
						experience={experience}
					/>
				))}

				<div className="group flex p-5">
					<Link to="/cv">
						<p className="text-[17px] leading-tight font-semibold text-slate-200 border-b-[1px] border-transparent hover:border-teal-300">
							Ver resumen completo
						</p>
					</Link>
					<FaArrowRight
						size={12}
						className="mt-[5px] ml-2 group-hover:ml-5 transition-all ease-in-out duration-200"
					/>
				</div>
			</div>
		</section>
	)
}

export default Experience
