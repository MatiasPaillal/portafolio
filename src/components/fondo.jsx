import React, { useEffect, useState } from 'react'

const Fondo = () => {
  const [mousePosition, setMousePosition] = useState({ x: 0, y: 0 })

  const handleMouseMove = (e) => {
    setMousePosition({ x: e.pageX, y: e.pageY })
  }

  useEffect(() => {
    const contePadre = document.getElementById('conte-padre')
    contePadre.addEventListener('mousemove', handleMouseMove)

    return () => {
      contePadre.removeEventListener('mousemove', handleMouseMove)
    }
  }, [])
  return (
    <div
      className="conte-degradado"
      style={{
        background: `radial-gradient(circle at ${mousePosition.x}px ${mousePosition.y}px, rgba(29, 78, 216, 0.15), transparent 450px)`,
      }}
    />
  )
}

export default Fondo
