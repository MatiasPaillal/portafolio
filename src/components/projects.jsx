import React from "react";
import portfolio from "../assets/portfolio.png";
import NormalIcons from "./icons/normalIcons";
import { MdArrowOutward } from "react-icons/md";

import DentalConnect from "../assets/dental_connect.png";

export const Projects = () => {
  return (
    <>
      <section
        data-aos="fade-down"
        className="w-full h-full gap-4"
        id="Proyectos"
      >
        <div className="w-full flex flex-col gap-5 mt-16" data-aos="fade-down">
          <a
            className="lg:flex p-5 transition-all group hover:bg-slate-800/50 hover:shadow-lg rounded-lg"
            href="https://gitlab.com/MatiasPaillal/portafolio"
            target="_blank"
          >
            <img
              className="mb-5 object-contain h-fit lg:w-[20%] ml-5 lg:m-0 w-[40%] rounded border-2 border-slate-200/10 transition group-hover:border-slate-200/30  "
              src={portfolio}
            />
            <div className="px-5 flex flex-col gap-2 lg:max-w-[80%]  max-w-full">
              <div className="flex ">
                <span className=" text-base text-md font-medium group-hover:text-teal-300">
                  Mi Portafolio
                </span>

                <MdArrowOutward
                  size={16}
                  className=" font-bold mt-1 ml-2 group-hover:text-teal-300 group-hover:mt-0 group-hover:ml-3 transition-all ease-in-out duration-200"
                />
              </div>

              <p className="text-slate-400 text-sm leading-normal ">
                Portafolio personal desarrollado con React, TailwindCSS y
                AOS.js. Este portafolio tiene como objetivo mostrar mis
                proyectos y experiencias de una manera simple y atractiva.
              </p>
              <div className="flex flex-wrap">
                <NormalIcons text={"React"} />
                <NormalIcons text={"JavaScript"} />
                <NormalIcons text={"Tailwind CSS"} />
              </div>
            </div>
          </a>
          <div
            className="lg:flex p-5 transition-all group hover:bg-slate-800/50 hover:shadow-lg rounded-lg"
            href="https://gitlab.com/users/MatiasPaillal/contributed"
          >
            <img
              className="mb-5 object-contain h-fit lg:w-[20%] ml-5 lg:m-0 w-[40%] rounded border-2 border-slate-200/10 transition group-hover:border-slate-200/30  "
              src={DentalConnect}
            />
            <div className="px-5 flex flex-col gap-2 lg:max-w-[80%]  max-w-full">
              <div className="flex ">
                <span className=" text-base font-medium text-md group-hover:text-teal-300">
                  Dental Connect
                </span>

                <MdArrowOutward
                  size={16}
                  className=" font-bold mt-1 ml-2 group-hover:text-teal-300 group-hover:mt-0 group-hover:ml-3 transition-all ease-in-out duration-200"
                />
              </div>

              <p className="text-slate-400 text-sm leading-normal ">
                Plataforma integral diseñada para facilitar la gestión de
                alquileres de espacios de trabajo destinada a odontólogos
                independientes, integrando además la atención a los pacientes.
                Aprovechando la potencia de la API de Google Maps, los usuarios
                pueden localizar fácilmente clínicas cercanas a su ubicación.
              </p>
              <div className="flex flex-wrap">
                <NormalIcons text={"React"} />
                <NormalIcons text={"JavaScript"} />
                <NormalIcons text={"NestJS"} />
                <NormalIcons text={"Typescript"} />
                <NormalIcons text={"Google Maps"} />
                <NormalIcons text={"Tailwind CSS"} />
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default Projects;
