import React, { useEffect, useState } from "react";
import curriculum from "../assets/curriculum.pdf";

const CV = () => {
  return (
    <iframe
      className="w-screen h-screen  "
      src={curriculum}
      title="PDF Preview"
    />
  );
};

export default CV;
