import React from "react";
import Projects from "./projects";
import Experience from "./experience";
import Tech from "./tech";
import Footer from "./footer";
export const Content = () => {
  return (
    <div className="grid place-items-center ">
      <div className="relative w-full h-full">
        <div className="relative flex flex-col gap-7 ">
          <Experience />
          <Projects />
          <Tech />
          <Footer />
        </div>
      </div>
    </div>
  );
};

export default Content;
