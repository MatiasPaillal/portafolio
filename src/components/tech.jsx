import React from 'react'
import { FaReact } from 'react-icons/fa'
import { IoLogoJavascript } from 'react-icons/io5'
import { BiLogoTypescript } from 'react-icons/bi'
import { FaJava } from 'react-icons/fa'
import { SiNextdotjs, SiTailwindcss } from 'react-icons/si'
import { FaBootstrap } from 'react-icons/fa'
import { SiSpringboot } from 'react-icons/si'
import { FaGithub } from 'react-icons/fa'
import { AiFillGitlab } from 'react-icons/ai'
import { FaFigma } from 'react-icons/fa'
import { SiMysql } from 'react-icons/si'
import { IoLogoFirebase } from 'react-icons/io5'

const tech = [
	<SiNextdotjs />,
	<FaReact className="" />,

	<BiLogoTypescript className="text-[43px]" />,
	<IoLogoJavascript className="rounded-[10px] " />,
	<h1 className="text-xl"> NestJS</h1>,
	<FaJava className="" />,
	<SiSpringboot className="" />,
	<IoLogoFirebase className="" />,
	<SiMysql className=" text-[50px]" />,
	<FaGithub className="" />,
	<AiFillGitlab className="" />,
	<FaFigma />,
	<SiTailwindcss className="" />,
	<FaBootstrap className="" />,
]

export const Tech = () => {
	return (
		<>
			<section
				data-aos="fade-down"
				className="w-full h-full gap-4"
				id="Tecnologías"
			>
				<div
					className="pl-7 px-[5%] items-center justify-center w-full grid grid-cols-5 gap-y-10 gap-x-2 my-16 text-[30px] text-slate-400	"
					data-aos="fade-down"
				>
					{tech.map((icon, index) => (
						<div
							key={index}
							className="flex items-center justify-center text-[40px]"
						>
							{icon}
						</div>
					))}
				</div>
			</section>
		</>
	)
}

export default Tech
