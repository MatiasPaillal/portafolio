function NormalIcons({ text }) {
  return (
    <div className="text-xs px-3 py-2 rounded-full font-medium  text-teal-300 bg-teal-400/10 mr-1.5 mt-2">
      <p className="leading-normal">{text}</p>
    </div>
  );
}
export default NormalIcons;
