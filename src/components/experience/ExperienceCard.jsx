import React from 'react'
import NormalIcons from '../icons/normalIcons'
import { MdArrowOutward } from 'react-icons/md'

const ExperienceCard = (props) => {
	const selectExperience = () => {
		const resolucion = window.innerWidth
		if (resolucion < 768) {
			return
		}
		props.setSelectedExperience(props.experience)
		props.setIsOpen(true)
	}

	return (
		<div>
			<div
				onClick={() => {
					selectExperience()
				}}
				key={props.experience.id}
				className=" text-sm lg:flex p-5 transition-all group hover:bg-slate-800/50 hover:shadow-lg rounded-lg "
				target="_blank"
			>
				<span className="mt-1 h-fit flex-wrap lg:w-[20%] ml-5 lg:m-0 w-[40%] font-bold uppercase tracking-wide text-slate-500 ">
					{props.experience.date}
				</span>
				<div className="px-5 flex flex-col gap-2 lg:max-w-[80%]  max-w-full relative">
					<div className="flex">
						<div className="font-medium leading-4 group-hover:text-teal-300 relative text-md">
							{props.experience.company}
							<div className="absolute -top-1  -right-7">
								<MdArrowOutward
									size={16}
									className=" font-bold mt-1 ml-2 group-hover:text-teal-300 group-hover:mt-0 group-hover:ml-3 transition-all ease-in-out duration-200"
								/>
							</div>
						</div>
					</div>
					<span className=" text-slate-500 font-medium">
						{props.experience.role}
					</span>

					<p className="text-slate-400 leading-normal mt-2 ">
						{props.experience.description}
					</p>
					<div className="flex flex-wrap mt-2">
						{props.experience.technologies.map((technologie, index) => (
							<div key={index}>
								<NormalIcons text={technologie} />
							</div>
						))}
					</div>
				</div>
			</div>
		</div>
	)
}

export default ExperienceCard
