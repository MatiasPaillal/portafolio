import React, { useEffect, useState } from 'react'
import { Modal, ModalContent } from 'keep-react'
import { IoMdClose } from 'react-icons/io'
import 'aos/dist/aos.css' // Importa los estilos de AOS
import LoadingSpinner from './LoadingSpinner'

const ExperienceCarousel = (props) => {
	const [experiences, setExperiences] = useState([])
	const [invisible, setinvisible] = useState(false)
	const [prevSelected, setPrevSelected] = useState({})
	const [translateX, setTranslateX] = useState(0)
	const [firstRender, setFirstRender] = useState(true)
	useEffect(() => {
		setExperiences(props.experiences)
	}, [props.experiences])

	useEffect(() => {
		const index = getIndexFromId(props.selectedExperience.id, props.experiences)
		const preExperiences = props.experiences.slice(0, index + 1)
		const postExperiences = props.experiences.slice(index + 1)
		setExperiences([...postExperiences, ...preExperiences])
		setTranslateX(0)
		if (firstRender) {
			setTimeout(() => {
				setFirstRender(false)
			}, 3000)
		}
	}, [props.isOpen])

	const slideWidth = 176

	const handleSlide = (experience) => {
		setPrevSelected(props.selectedExperience)
		props.setSelectedExperience(experience)
		setinvisible(true)

		setTimeout(() => {
			if (Object.keys(props.selectedExperience).length > 0) {
				setTranslateX((prev) => {
					const newTranslate = prev - slideWidth
					return newTranslate
				})
				setExperiences([...experiences, experience])
			}
		}, 600)
		setTimeout(() => {
			setinvisible(false)
		}, 1200)
	}

	const getIndexFromId = (id, experiences) => {
		return experiences.findIndex((experience) => experience.id === id)
	}

	const getIndexExperiences = (experience) => {
		const index = getIndexFromId(experience.id, props.experiences)
		const selectedIndex = getIndexFromId(
			props.selectedExperience.id,
			props.experiences
		)
		const previousIndex =
			selectedIndex > 0 ? selectedIndex - 1 : props.experiences.length - 1
		const nextIndex =
			selectedIndex < props.experiences.length - 1 ? selectedIndex + 1 : 0

		return {
			prev: previousIndex,
			sel: selectedIndex,
			next: nextIndex,
			index: index,
		}
	}
	const getZIndexExperiences = (experience) => {
		const res = getIndexExperiences(experience)
		if (res.sel === res.index) {
			return '-z-10'
		} else if (res.next === res.index) {
			return 'z-0'
		} else {
			return '-z-30'
		}
	}

	return (
		<div>
			<Modal open={props.isOpen} showCloseIcon={false}>
				<ModalContent
					className={`focus:outline-none  focus:ring-0 focus:border-transparent p-0 border-0 text-slate-200 min-w-[90vw] min-h-[90vh] bg-slate-900 flex  justify-center`}
				>
					{firstRender && (
						<div className="absolute top-0 left-0 w-full h-full bg-slate-900 z-50 flex items-center justify-center gap-4">
							<LoadingSpinner />
							Cargando...
						</div>
					)}
					<div className=" relative text-sm rounded-xl bg-contain bg-center bg-no-repeat">
						{props.experiences.map((experience) => (
							<img
								data-aos={
									invisible && experience.id != props.selectedExperience.id
										? 'fade-left'
										: ''
								}
								key={experience.id}
								src={experience?.image}
								className={`${
									experience.id === props.selectedExperience.id
										? ' w-full h-full m-0 transition-all duration-1000'
										: ' w-[160px] h-[100px] mr-[222px] mb-16 transition-all duration-1000'
								} absolute bottom-0 right-0   ${getZIndexExperiences(
									experience
								)}

                                ${
																	invisible &&
																	experience.id != props.selectedExperience.id
																		? 'invisible'
																		: 'visible'
																}
                                object-fill rounded-xl transition-all duration-500`}
							/>
						))}

						<img
							key={prevSelected.id}
							src={prevSelected?.image}
							className=" w-full h-full absolute top-0 right-0 -z-20  object-fill rounded-xl pointer-events-auto"
						/>
						<div className=" p-8 flex w-full h-full md:flex-row flex-col justify-between ">
							<div
								onClick={() => props.setIsOpen(false)}
								className="absolute top-5 text-slate-400 right-5 bg-slate-700 z-50 w-6 h-6 rounded-full flex items-center justify-center"
							>
								<IoMdClose className="w-6 h-6" />
							</div>

							{props.experiences.map(
								(experience) =>
									experience.id === props.selectedExperience.id && (
										<div
											key={experience.id}
											className="md:pb-8 md:w-[30%] md:h-full w-full h-fit flex items-end"
											data-aos="fade-up"
										>
											<div
												key={experience.id}
												id={props.selectedExperience.id}
												data-aos="fade-up"
												className={`  ${
													props.selectedExperience.theme == 'light'
														? 'text-white'
														: 'text-slate-600'
												} flex flex-col justify-between`}
											>
												<span className="text-xs mb-4">
													{props.selectedExperience.date}
												</span>
												<div className="flex mb-2">
													<div className="text-2xl font-bold leading-7 group-hover:text-teal-300 relative">
														{props.selectedExperience.company}
													</div>
												</div>
												<span className="font-medium text-md mb-4">
													{props.selectedExperience.role}
												</span>

												<p className="leading-normal mt-2 min-w-[400px] ">
													{props.selectedExperience.description} <br />
													{props?.selectedExperience?.summary}
												</p>

												<div className="flex flex-wrap gap-2 mt-4">
													{props.selectedExperience.technologies.map(
														(tech, index) => (
															<span
																key={index}
																className="text-white text-xs bg-slate-700 rounded-lg px-2 py-1 mr-2"
															>
																{tech}
															</span>
														)
													)}
												</div>
											</div>
										</div>
									)
							)}

							<div className="md:w-[64%] w-full md:h-full h-fit flex justify-end items-end z-10">
								<div className="flex pb-8 w-[350px] overflow-x-hidden ">
									<div
										className="flex gap-4 transition-transform duration-500"
										style={{ transform: `translateX(${translateX}px)` }}
									>
										{experiences.map((experience, index) => (
											<div
												key={index}
												className={`${
													invisible &&
													experience.id === props.selectedExperience.id
														? 'opacity-0'
														: ''
												} w-[160px] h-[100px] bg-slate-800 rounded-lg shadow-[5px_5px_5px_rgba(0,0,0,0.5)]
                                                
                                                ${
																									index !=
																									experiences.length -
																										props.experiences.length
																										? 'pointer-events-none   '
																										: 'border-slate-400 border-2'
																								}
                                                `}
												style={{
													backgroundImage: `url(${experience.image})`,
													backgroundSize: 'cover',
													backgroundPosition: 'center',
												}}
												onClick={() =>
													invisible ? null : handleSlide(experience)
												}
											/>
										))}
									</div>
								</div>
							</div>
						</div>
					</div>
				</ModalContent>
			</Modal>
		</div>
	)
}

export default ExperienceCarousel
