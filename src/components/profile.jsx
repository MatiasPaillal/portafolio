import React from 'react'
import 'aos/dist/aos.css'
import Navbar from './navbar'
import { FaInstagram, FaGithub, FaLinkedin } from 'react-icons/fa'
import { BiLogoGmail } from 'react-icons/bi'

export const Profile = () => {
	return (
		<>
			<div className="sm:h-screen w-full  justify-center flex sm:sticky static top-0 z-10">
				<div className=" px-9 py-24">
					<div className="body flex flex-col gap-[80px]">
						<div className="flex flex-col gap-[10px]">
							<h1 className="text-[48px] tracking-tight font-bold ">
								Matias Paillal
							</h1>

							<h1 className="text-lg tracking-tight ">
								Estudiante y Desarrollador de Software
							</h1>
							<p className=" text-slate-400 max-w-[85%]">
								Enfocado en crear soluciones digitales y experiencias de usuario
								intuitivas.
							</p>
						</div>

						<Navbar />
						<div className="flex w-[200px] mt-6">
							<a
								target="_blank"
								href="https://www.linkedin.com/in/matías-paillal-219770302"
								className="p-1 hover:bg-teal-300/20 transition-all rounded-full mr-3"
							>
								<FaLinkedin
									size={26}
									color="rgb(148 163 184 / var(--tw-text-opacity))"
								/>
							</a>
							<a
								target="_blank"
								href="https://github.com/MatiasPaillal"
								className="p-1 hover:bg-teal-300/20 transition-all rounded-full mr-3"
							>
								<FaGithub
									size={26}
									color="rgb(148 163 184 / var(--tw-text-opacity))"
								/>
							</a>
							<a
								target="_blank"
								href="https://mail.google.com/mail/u/1/#inbox?compose=DmwnWsTMDgvXdmGZVDBNThdxWFRCVslLCvlhXpHssFJzKgZtnzgMzmmpTRcXbdNbRPTJjmBDzfZQ"
								className="p-1 hover:bg-teal-300/20 transition-all rounded-full mr-3"
							>
								<BiLogoGmail
									size={26}
									color="rgb(148 163 184 / var(--tw-text-opacity))"
								/>
							</a>{' '}
						</div>
					</div>
				</div>
			</div>
		</>
	)
}

export default Profile
