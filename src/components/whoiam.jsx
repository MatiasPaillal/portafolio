import React, { useState } from 'react'

import { FaInstagram, FaGithub } from 'react-icons/fa'

import { BiLogoGmail } from 'react-icons/bi'

import {
	Button,
	Modal,
	ModalAction,
	ModalContent,
	ModalDescription,
	ModalFooter,
	ModalHeader,
	ModalTitle,
} from 'keep-react'
import { Link } from 'react-router-dom'
export const Whoiam = () => {
	return (
		<>
			<div className=" z-2 h-screen overflow-hidden relative before:content-[''] before:absolute before:bg-gradient-to-b before:from-transparent before:to-slate-900 opacity-90 before:w-[100%] before:h-[40%] right:top-0 before:top-[60%]">
				<div
					className=" justify-center sm:pl-24 absolute top-0 right-30 p-5 h-screen  flex flex-col items-start  gap-2"
					data-aos="fade-down"
				>
					<div
						id="info"
						className=" min-w-[400px] flex flex-col  backdrop-blur-[2px] bg-white/10  gap-3 p-6 pr-12  rounded-lg border border-white/5 "
					>
						<p className="uppercase text-[2vw] leading-none font-bold tracking-tight equinox    ">
							Hola, soy
						</p>

						<p className=" leading-none text-[4vw] font-medium tracking-tighter equinox  ">
							Matias
						</p>
						<div className="type">
							<p className="uppercase text-[2vw] leading-none font-bold tracking-tight equinox  ">
								Ingeniero en Informática
							</p>
						</div>
					</div>
					<div className="mt-1 flex gap-2">
						<a
							className="cybr-btn h-[75px] items-center flex group"
							href="https://github.com/MatiasPaillal"
							target="_blank"
						>
							<span
								className="text-white w-full flex justify-center group-hover:text-slate-900"
								aria-hidden
							>
								<FaGithub size={35} />
							</span>
							<span aria-hidden className="cybr-btn__glitch relative">
								<div className="absolute top-0 w-full h-full flex justify-center items-center">
									<FaGithub size={35} />
								</div>
							</span>
						</a>

						<a
							className="cybr-btn  items-center flex group "
							target="_blank"
							href="https://mail.google.com/mail/u/1/#inbox?compose=DmwnWsTMDgvXdmGZVDBNThdxWFRCVslLCvlhXpHssFJzKgZtnzgMzmmpTRcXbdNbRPTJjmBDzfZQ"
						>
							<span
								className="text-white w-full flex justify-center group-hover:text-slate-900"
								aria-hidden
							>
								<BiLogoGmail size={35} />
							</span>
							<span aria-hidden className="cybr-btn__glitch">
								<div className="absolute top-0 w-full h-full flex justify-center items-center">
									<BiLogoGmail size={35} />
								</div>
							</span>
						</a>

						<Link
							to="/cv"
							className="cybr-btn  items-center flex group"
							target="_blank"
							href="https://mail.google.com/mail/u/1/#inbox?compose=DmwnWsTMDgvXdmGZVDBNThdxWFRCVslLCvlhXpHssFJzKgZtnzgMzmmpTRcXbdNbRPTJjmBDzfZQ"
						>
							<span
								className="text-white w-full flex justify-center group-hover:text-slate-900"
								aria-hidden
							>
								CV
							</span>
							<span aria-hidden className="cybr-btn__glitch">
								<div className="absolute top-0 w-full h-full flex justify-center items-center">
									CV
								</div>
							</span>
						</Link>
					</div>
				</div>
				<button
					className="absolute w-screen flex h-40 items-center justify-center bottom-0 z-[51] "
					onClick={() => {
						const navbar = document.getElementById('Experiencia')
						navbar.scrollIntoView({
							behavior: 'smooth',
							block: 'end',
							inline: 'end',
						})
					}}
				>
					<div className=" border-2 border-white animate-bounce bg-slate-800 dark:bg-red-800 p-2 w-10 h-10 ring-1 ring-slate-900/5 dark:ring-slate-200/20 shadow-lg rounded-full flex items-center justify-center">
						<svg
							className="w-6 h-6 text-white"
							fill="none"
							strokeLinecap="round"
							strokeLinejoin="round"
							strokeWidth="2"
							viewBox="0 0 24 24"
							stroke="currentColor"
						>
							<path d="M19 14l-7 7m0 0l-7-7m7 7V3"></path>
						</svg>
					</div>
				</button>
			</div>
		</>
	)
}

export default Whoiam
