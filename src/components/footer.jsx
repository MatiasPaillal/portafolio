import React from "react";
export const Footer = () => {
  return (
    <>
      <div data-aos="fade-down" className="w-full flex justify-center px-8">
        <p className="mb-24 w-full text-sm text-slate-400">
          Diseñado con Figma y codificado en Visual Studio Code. Construido con
          Vite, React y Tailwind CSS, y desplegado con Netlify. La fuente
          utilizada corresponde a Inter.
        </p>
      </div>
    </>
  );
};

export default Footer;
