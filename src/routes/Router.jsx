import { createBrowserRouter } from "react-router-dom";
import CV from "../components/CV";
// Importaciones de las vistas

import Portafolio from "../views/portfolio";
const router = createBrowserRouter([
  {
    path: "/",
    element: <Portafolio />,
  },
  {
    path: "/cv",
    element: <CV />,
  },
]);
export default router;
