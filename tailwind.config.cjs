import { keepTheme } from 'keep-react/keepTheme'
/** @type {import('tailwindcss').Config} */
module.exports = keepTheme({
	content: ['./index.html', './src/**/*.{js,ts,jsx,tsx}'],
	theme: {
		fontSize: {
			xs: '0.7rem',
			sm: '0.8rem',
			md: '0.9rem',
			lg: '1rem',
			xl: '1.25rem',
			'2xl': '1.75rem',
		},
		screens: {
			sm: '640px',
			// => @media (min-width: 640px) { ... }

			md: '768px',
			// => @media (min-width: 768px) { ... }

			lg: '1024px',
			// => @media (min-width: 1024px) { ... }

			xl: '1280px',
			// => @media (min-width: 1280px) { ... }

			'2xl': '1536px',
			// => @media (min-width: 1536px) { ... }
		},
		extend: {
			backgroundImage: {
				fondo: "url('/src/assets/astro2.jpg')",
			},
		},
	},
	plugins: [],
})
